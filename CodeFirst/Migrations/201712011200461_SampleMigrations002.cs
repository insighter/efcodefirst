namespace CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SampleMigrations002 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "LastName", c => c.String());
            AlterColumn("dbo.Customers", "FirstName", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Customers", "FirstName", c => c.String(maxLength: 20));
            DropColumn("dbo.Customers", "LastName");
        }
    }
}
