﻿using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFCodeFirst
{
    public class SampleContext : DbContext
    {
        // Имя будущей базы данных можно указать через
        // вызов конструктора базового класса
        public SampleContext() : base("MyShop")
        { }

        // Отражение таблиц базы данных на свойства с типом DbSet
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Project> Projects { get; set; }


        // Переопределяем метод OnModelCreating для добавления
        // настроек конфигурации
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Configurations.Add(new CustomerConfiguration());
            //modelBuilder.Configurations.Add(new OrderConfiguration());

            //modelBuilder.Entity<Customer>().Property(c => c.FirstName)
            //      .HasMaxLength(30);

            //modelBuilder.Entity<Customer>().Property(c => c.LastName)
            //      .HasMaxLength(30);

            //modelBuilder.Entity<Customer>().Property(c => c.)
            //    .HasMaxLength(100);

            //modelBuilder.Entity<Order>().Property(o => o.ProductName)
            //    .HasMaxLength(500);

            //modelBuilder.Entity<Customer>().Property(c => c.Photo)
            //    .HasColumnType("image");

            //modelBuilder.Entity<Customer>().Property(c => c.Name)
            //      .IsRequired();

            //modelBuilder.Entity<Project>().HasKey(p => p.Identifier);

            //modelBuilder.Entity<Project>().Property(p => p.Identifier)
            //      .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }    
}