﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Web;
using System.Web.UI;
using System.Web.ModelBinding;
using System.IO;

namespace EFCodeFirst
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                //Customer customer = new Customer();

                //Database.SetInitializer(
                //    new DropCreateDatabaseAlways<SampleContext>());

                SampleContext context = new SampleContext();

                var customer = new Customer
                {
                };

                context.Customers.Add(customer);
                context.SaveChanges();

                var order = new Order
                {
                    PurchaseDate = DateTime.UtcNow
                };

                context.Orders.Add(order);
                context.SaveChanges();



                //// Получить данные из формы с помощью средств
                //// привязки моделей ASP.NET
                //IValueProvider provider =
                //    new FormValueProvider(ModelBindingExecutionContext);
                //if (TryUpdateModel<Customer>(customer, provider))
                //{
                //    // Загрузить фото профиля с помощью средств .NET
                //    HttpPostedFile photo = Request.Files["photo"];
                //    if (photo != null)
                //    {
                //        BinaryReader b = new BinaryReader(photo.InputStream);
                //        customer.Photo = b.ReadBytes((int)photo.InputStream.Length);
                //    }

                //    // В этой точке непосредственно начинается работа с Entity Framework

                //    // Создать объект контекста
                //    SampleContext context = new SampleContext();

                //    // Вставить данные в таблицу Customers с помощью LINQ
                //    context.Customers.Add(customer);

                //    // Сохранить изменения в БД
                //    context.SaveChanges();
                //}
            }
        }
    }
}